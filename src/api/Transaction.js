const uuid = require('uuid/v4');


class Transaction {
  constructor(id, fromAccountId, toAccountId, amount, category, creationDate) {
    this.id = id || uuid();
    this.creationDate = creationDate || Date.now();
    this.updateDate = Date.now();
    this.fromAccountId = fromAccountId;
    this.toAccountId = toAccountId;
    this.amount = amount;
    this.category = category;
    this.executed = false;
  }

  execute() {
    // TODO: get rid of the require here
    const bank = require('./Bank');

    const fromAccount = bank.getAccountById(this.fromAccountId);
    const toAccount = bank.getAccountById(this.toAccountId);

    fromAccount.debit(this.amount);
    toAccount.credit(this.amount);

    this.executed = true;
    this.payee = toAccount.name;

    fromAccount.addTx(this.txDetails);
    toAccount.addTx(this.txDetails);

    return this;
  }

  get txDetails() {
    return {
      id: this.id,
      creationDate: this.creationDate,
      updateDate: this.updateDate,
      fromAccountId: this.fromAccountId,
      toAccountId: this.toAccountId,
      payee: this.payee,
      amount: this.amount,
      executed: this.executed,
    };
  }
}

module.exports = Transaction;
