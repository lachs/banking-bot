const uuid = require('uuid/v4');
const Account = require('./Account');

class Bank {
  constructor(name, currency = 'EUR') {
    this.name = name || 'Demobank';
    this.currency = currency;
    this.accounts = [];
  }

  createAccount(amount, name = null, id = uuid()) {
    const acc = new Account(id, name, amount);
    this.accounts.push(acc);
    return acc;
  }

  getAccountById(id) {
    return this.accounts.find(acc => acc.id === id);
  }

  getAccountByName(name) {
    return this.accounts.find(acc => acc.name === name);
  }
}

module.exports = new Bank();
