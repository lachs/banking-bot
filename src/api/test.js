const bank = require('./Bank');

console.log('## Create new bank');
console.log(bank);
console.log();

console.log('## Create two accounts');
bank.createAccount(20, 'Cash', 1);
bank.createAccount(20, 'KKF', 2);
console.log(bank);
console.log();

console.log('## Send 10 moneys from account 1 to 2');
bank.getAccountByName('Cash').sendTx(2, 10);
console.log(bank);
console.log();

console.log('## Get transactions of both accs');
console.log(bank.getAccountById(1).transactions);
console.log(bank.getAccountById(2).transactions);
console.log();

console.log('## Send the money back');
bank.getAccountById(2).sendTx(1, 10);
console.log(bank);
console.log();

console.log('## Get transactions of both accs');
console.log(bank.getAccountById(1).transactions);
console.log(bank.getAccountById(2).transactions);
