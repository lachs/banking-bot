const Transaction = require('./Transaction');
const round = require('../helpers/round.js');

class Account {
  constructor(id, name, balance = 0) {
    this.id = id;
    this.name = name;
    this.balance = balance;
    this.transactions = [];
  }

  static get balance() {
    return this.balance;
  }

  static set balance(x) {
    this.balance = x;
    return this.balance;
  }

  name() {
    return this.name;
  }

  debit(x) {
    this.balance -= x;
    this.balance = round(this.balance);
    return this;
  }

  credit(x) {
    this.balance += x;
    this.balance = round(this.balance);
    return this;
  }

  addTx(t) {
    this.transactions.push(t);
  }

  static get transactions() {
    return this.transactions;
  }

  sendTx(toAccount, amount, creationDate) {
    return new Transaction(null, this.id, toAccount, amount, null, creationDate).execute();
  }
}

module.exports = Account;
