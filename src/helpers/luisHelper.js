// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { LuisRecognizer } = require('botbuilder-ai');
const { valueResolver } = require('@microsoft/recognizers-text-data-types-timex-expression');

class LuisHelper {
  /**
     * Returns an object with preformatted LUIS results for the bot's dialogs to consume.
     * @param {*} logger
     * @param {TurnContext} context
     */
  static async executeLuisQuery(logger, context) {
    const result = {};
    let intent;

    try {
      const recognizer = new LuisRecognizer({
        applicationId: process.env.LuisAppId,
        endpointKey: process.env.LuisAPIKey,
        endpoint: `https://${process.env.LuisAPIHostName}`,
      }, {}, true);

      const recognizerResult = await recognizer.recognize(context);

      intent = LuisRecognizer.topIntent(recognizerResult);


      if (intent === 'None') {
        return {};
      }
      if (intent === 'Money_Spend') {
        // We need to get the result from the LUIS JSON which at every level returns an array

        result.payee = LuisHelper.parseEntity(recognizerResult, 'Account');
        result.amount = LuisHelper.parseEntity(recognizerResult, 'money').number;
        result.products = LuisHelper.parseEntity(recognizerResult, 'Product');
        result.datetime = LuisHelper.parseDatetimeEntity(recognizerResult);
      }
      if (intent === 'Account_Balance') {
        result.account = LuisHelper.parseEntity(recognizerResult, 'Account');
        console.log('### LUIS: recognized account\n', result.account);
      }
      if (intent === 'Money_Stats') {
        console.log('### LUIS: recognized Money_Stats\n', recognizerResult.entities);

        result.payee = LuisHelper.parseEntity(recognizerResult, 'Account');
        result.datetime = LuisHelper.parseDatetimeEntity(recognizerResult);
      }
    } catch (err) {
      logger.warn(`LUIS Exception: ${err} Check your LUIS configuration`);
    }
    return { data: result, intent };
  }

  static parseEntity(result, entityName) {
    if (result.entities[entityName]) {
      return result.entities[entityName][0];
    }
    return undefined;
  }

  static parseCompositeEntity(result, compositeName, entityName) {
    const compositeEntity = result.entities[compositeName];
    if (!compositeEntity || !compositeEntity[0]) return undefined;

    const entity = compositeEntity[0][entityName];
    if (!entity || !entity[0]) return undefined;

    const entityValue = entity[0][0];
    return entityValue;
  }

  static parseDatetimeEntity(result) {
    const datetimeEntity = result.entities.datetime;
    if (!datetimeEntity || !datetimeEntity[0]) return undefined;

    const { timex } = datetimeEntity[0];
    if (!timex || !timex[0]) return undefined;

    const datetime = valueResolver.resolve(timex).values;
    return datetime;
  }
}

module.exports.LuisHelper = LuisHelper;
