// https://www.jacklmoore.com/notes/rounding-in-javascript/
const round = (value, decimals = 2) => Number(Math.round(value + 'e' + decimals) + 'e-' + decimals); // eslint-disable-line prefer-template

module.exports = round;
