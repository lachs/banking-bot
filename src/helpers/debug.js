const bank = require('../api/Bank');
const round = require('./round');

function rand() {
  return round(Math.random() * 10);
}

const cashAcc = bank.createAccount(250, 'cash');
const reweAcc = bank.createAccount(0, 'rewe');
const kkfAcc = bank.createAccount(0, 'kkf');
const mensaAcc = bank.createAccount(0, 'mensa');

cashAcc.sendTx(reweAcc.id, rand());
cashAcc.sendTx(reweAcc.id, rand());
cashAcc.sendTx(reweAcc.id, rand());

cashAcc.sendTx(kkfAcc.id, rand());
cashAcc.sendTx(kkfAcc.id, rand());
cashAcc.sendTx(kkfAcc.id, rand());

cashAcc.sendTx(mensaAcc.id, rand());
cashAcc.sendTx(mensaAcc.id, rand());
cashAcc.sendTx(mensaAcc.id, rand());

cashAcc.sendTx(reweAcc.id, rand(), new Date('2019-05-14'));
cashAcc.sendTx(kkfAcc.id, rand(), new Date('2019-05-14'));
cashAcc.sendTx(mensaAcc.id, rand(), new Date('2019-05-14'));

cashAcc.sendTx(reweAcc.id, rand(), new Date('2019-05-10'));
cashAcc.sendTx(kkfAcc.id, rand(), new Date('2019-05-10'));
cashAcc.sendTx(mensaAcc.id, rand(), new Date('2019-05-10'));
