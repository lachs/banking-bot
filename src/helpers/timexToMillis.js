function timexToMillis(t) {
  if (t.type === 'daterange') {
    // timex to unix timestamp
    const start = new Date(t.start).getTime();
    const end = new Date(t.end).getTime();

    return {
      timex: t.timex,
      start,
      end,
      values: t.values,
    };
  }
  const start = new Date(t.value).getTime();
  // add 24 hrs for date range
  const end = (new Date(t.value).getTime() + (60 * 60 * 24 * 1000));

  return {
    timex: t.timex,
    start,
    end,
    values: t.values,
  };
}

module.exports = timexToMillis;

// let res = timexToMillis([{
//   timex: '2019-05',
//   type: 'daterange',
//   start: '2019-05-01',
//   end: '2019-06-01',
// }]);
// console.log(res);
// res = timexToMillis([{ timex: '2019-05-14', type: 'date', value: '2019-05-14' }]);
// console.log(res);
