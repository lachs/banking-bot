// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  ComponentDialog,
  DialogSet,
  DialogTurnStatus,
  TextPrompt,
  WaterfallDialog,
} = require('botbuilder-dialogs');

const NAME_PROMPT = 'NAME_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

class AccountBalanceDialog extends ComponentDialog {
  constructor(bankAccessor, logger, bank) {
    super('Account_Balance');

    this.bankAccessor = bankAccessor;

    this.logger = logger;

    this.bank = bank;

    this.addDialog(new TextPrompt(NAME_PROMPT));

    this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
      this.accountStep.bind(this),
      this.summaryStep.bind(this),
    ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
   * If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  async accountStep(step) {
    const accDetails = step.options;

    if (!accDetails.account) {
      return await step.prompt(NAME_PROMPT, 'Please tell me the name of the account.');
    }
    return await step.next(accDetails.account);
  }

  async summaryStep(step) {
    if (step.result) {
      const accDetails = step.options;

      const { bank } = this;
      const { balance } = bank.getAccountByName(accDetails.account);

      await step.context.sendActivity(`You got ${balance}€ on ${accDetails.account}`);
      return await step.endDialog();
    }
    return await step.endDialog();
  }
}

module.exports.AccountBalanceDialog = AccountBalanceDialog;
