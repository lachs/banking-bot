// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog,
} = require('botbuilder-dialogs');
const { CreateBankDialog } = require('./createBankDialog');
const { MoneySpendDialog } = require('./moneySpendDialog');
const { AccountBalanceDialog } = require('./accountBalanceDialog');
const { MoneyStatsDialog } = require('./moneyStatsDialog');
const { LuisHelper } = require('../helpers/luisHelper');
const bank = require('../api/Bank');

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line global-require
  require('../helpers/debug');
}

const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';

class MainDialog extends ComponentDialog {
  constructor(userState, logger) {
    super('MainDialog');

    if (!logger) {
      logger = console;
      logger.log('[MainDialog]: logger not passed in, defaulting to console');
    }

    this.bankAccessor = userState.createProperty('BANK');
    this.userState = userState;

    this.logger = logger;

    // Define the main dialog and its related components.
    // This is a sample "book a flight" dialog.
    this.addDialog(new TextPrompt('TextPrompt'))
      .addDialog(new CreateBankDialog(this.bankAccessor, logger, bank))
      .addDialog(new MoneySpendDialog(this.bankAccessor, logger, bank))
      .addDialog(new AccountBalanceDialog(this.bankAccessor, logger, bank))
      .addDialog(new MoneyStatsDialog(this.bankAccessor, logger, bank))
      .addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.bankStep.bind(this),
        this.actStep.bind(this),
        this.finalStep.bind(this),
      ]));

    this.initialDialogId = MAIN_WATERFALL_DIALOG;
  }

  /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  /**
     * First step in the waterfall dialog. Prompts the user for a command.
     * Currently, this expects a booking request, like "book me a flight from Paris to Berlin on march 22"
     * Note that the sample LUIS model will only recognize Paris, Berlin, New York and London as airport cities.
     */
  async introStep(step) {
    if (!process.env.LuisAppId || !process.env.LuisAPIKey || !process.env.LuisAPIHostName) {
      await step.context.sendActivity('NOTE: LUIS is not configured. To enable all capabilities, add `LuisAppId`, `LuisAPIKey` and `LuisAPIHostName` to the .env file.');
      return await step.endDialog();
    }
    return await step.next();
  }

  async bankStep(step) {
    // const hasBank = await this.bankAccessor.get(step.context, false);
    const hasBank = bank.accounts[0];
    console.log('### Bank\n', bank);

    if (!hasBank) {
      return await step.beginDialog('createBankDialog');
    }
    return await step.next();
  }

  /**
     * Second step in the waterall.  This will use LUIS to attempt to extract the origin, destination and travel dates.
     * Then, it hands off to the bookingDialog child dialog to collect any remaining details.
     */
  async actStep(step) {
    let luisDetails = {};

    if (process.env.LuisAppId && process.env.LuisAPIKey && process.env.LuisAPIHostName) {
      // Call LUIS and gather any potential booking details.
      // This will attempt to extract the origin, destination and travel date from the user's message
      // and will then pass those values into the booking dialog
      luisDetails = await LuisHelper.executeLuisQuery(this.logger, step.context);

      this.logger.log('LUIS extracted these details:', luisDetails);
    }

    if (luisDetails.intent) {
      return await step.beginDialog(luisDetails.intent, luisDetails.data);
    }
    return await step.next();
  }

  /**
     * This is the final step in the main waterfall dialog.
     * It wraps up the sample "book a flight" interaction with a simple confirmation.
     */
  async finalStep(step) {
    const msg = `What can I do? You can ask me...
    * for account balances, including payee accounts
    * to create a new account transaction
    * for spending statistics – accumulated or per payee
    `;
    await step.context.sendActivity(msg);
    return await step.endDialog();
  }
}

module.exports.MainDialog = MainDialog;
