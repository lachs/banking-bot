// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  ComponentDialog,
  ConfirmPrompt,
  DateTimePrompt,
  DialogSet,
  DialogTurnStatus,
  WaterfallDialog,
} = require('botbuilder-dialogs');
const _ = require('lodash');

const timexToMillis = require('../helpers/timexToMillis');
const round = require('../helpers/round');

const CONFIRM_PROMPT = 'CONFIRM_PROMPT';
const DATETIME_PROMPT = 'DATETIME_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

class MoneyStatsDialog extends ComponentDialog {
  constructor(bankAccessor, logger, bank) {
    super('Money_Stats');

    this.bankAccessor = bankAccessor;

    this.logger = logger;

    this.bank = bank;

    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
    this.addDialog(new DateTimePrompt(DATETIME_PROMPT));

    this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
      this.datetimeStep.bind(this),
      this.payeeStep.bind(this),
      this.summaryStep.bind(this),
      this.topPayeesStep.bind(this),
    ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
   * If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  async datetimeStep(step) {
    const statsDetails = step.options;

    if (!statsDetails.datetime) {
      return await step.next();
    }
    return await step.next(statsDetails.account);
  }

  async payeeStep(step) {
    const statsDetails = step.options;

    if (!statsDetails.payee) {
      return await step.next();
    }
    return await step.next(statsDetails.payee);
  }

  async summaryStep(step) {
    const statsDetails = step.options;

    if (!statsDetails.datetime) {
      statsDetails.datetime = step.result;
    }
    console.log('### summaryStep statsDetails\n', statsDetails);
    const datetime = timexToMillis(statsDetails.datetime[0]);
    console.log('### summaryStep datetime\n', datetime);

    const { bank } = this;
    const { transactions } = bank.getAccountByName('cash');
    console.log('### summaryStep account transactions\n', transactions);

    const balance = transactions
      .filter(t => t.creationDate >= datetime.start && t.creationDate < datetime.end)
      .filter((t) => {
        if (statsDetails.payee) {
          return t.payee === statsDetails.payee;
        }
        return true;
      })
      .reduce((acc, t) => acc + t.amount, 0);

    console.log('### summaryStep total balance\n', balance);

    await step.context.sendActivity(`Stats from ${new Date(datetime.start).toLocaleDateString()} to ${new Date(datetime.end).toLocaleDateString()}`);
    await step.context.sendActivity(`You spent ${round(balance)}€`);
    // await step.context.sendActivity(JSON.stringify(transactions));
    // return await step.endDialog();
    return await step.prompt(CONFIRM_PROMPT, 'Wanna see the top payees?');
  }

  // FIXME: this method looks like shit. make it nice
  async topPayeesStep(step) {
    if (step.result === true) {
      const statsDetails = step.options;
      const datetime = timexToMillis(statsDetails.datetime[0]);

      const { transactions } = this.bank.getAccountByName('cash');

      const filteredTxns = transactions
        .filter(t => t.creationDate >= datetime.start && t.creationDate < datetime.end);


      const arrayToObject = (array, keyField) => {
        return array.reduce((obj, item) => {
          obj[item[keyField]] = obj[item[keyField]] + item.amount || item.amount;
          return obj;
        }, {});
      };

      const reduced = _.toPairs(arrayToObject(filteredTxns, 'payee'));
      const sorted = reduced.sort((a, b) => b[1] - a[1]).slice(0, 5);

      console.log('### topPayees sorted array\n', sorted);
      const promises = [];
      sorted.forEach((val) => {
        promises.push(step.context.sendActivity(`${val[0]}: ${round(val[1])}€`));
      });
      await Promise.all(promises);
      return await step.endDialog();
    }
    return await step.endDialog();
  }
}

module.exports.MoneyStatsDialog = MoneyStatsDialog;
