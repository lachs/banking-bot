// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  ComponentDialog,
  ConfirmPrompt,
  DialogSet,
  DialogTurnStatus,
  NumberPrompt,
  TextPrompt,
  WaterfallDialog,
} = require('botbuilder-dialogs');

const timexToMillis = require('../helpers/timexToMillis');

const CONFIRM_PROMPT = 'CONFIRM_PROMPT';
const ACCOUNT_PROMPT = 'ACCOUNT_PROMPT';
const AMOUNT_PROMPT = 'AMOUNT_PROMPT';
const PRODUCT_PROMPT = 'PRODUCT_PROMPT';
const DATETIME_PROMPT = 'DATETIME_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

class MoneySpendDialog extends ComponentDialog {
  constructor(bankAccessor, logger, bank) {
    super('Money_Spend');

    this.bankAccessor = bankAccessor;

    this.logger = logger;

    this.bank = bank;

    this.addDialog(new TextPrompt(ACCOUNT_PROMPT));
    this.addDialog(new NumberPrompt(AMOUNT_PROMPT));
    this.addDialog(new TextPrompt(PRODUCT_PROMPT));
    this.addDialog(new TextPrompt(DATETIME_PROMPT));
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));

    this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
      this.accountStep.bind(this),
      this.amountStep.bind(this),
      this.productStep.bind(this),
      this.datetimeStep.bind(this),
      this.confirmStep.bind(this),
      this.summaryStep.bind(this),
    ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
   * If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  async accountStep(step) {
    console.log(step.options);
    const txDetails = step.options;

    if (!txDetails.payee) {
      return await step.prompt(ACCOUNT_PROMPT, 'To whom did you pay the money?');
    }
    return await step.next(txDetails.payee);
  }

  async amountStep(step) {
    console.log(step.options);
    const txDetails = step.options;

    txDetails.payee = step.result;
    if (!txDetails.amount) {
      return await step.prompt(AMOUNT_PROMPT, 'How much?');
    }
    return await step.next(txDetails.amount);
  }

  async productStep(step) {
    console.log(step.options);
    const txDetails = step.options;

    txDetails.amount = step.result;
    if (!txDetails.products) {
      return await step.prompt(PRODUCT_PROMPT, 'So, for what...?');
    }
    return await step.next(txDetails.products);
  }

  async datetimeStep(step) {
    console.log(step.options);
    const txDetails = step.options;

    txDetails.products = step.result;
    if (!txDetails.datetime) {
      return await step.next(null);
    }
    return await step.next(txDetails.datetime);
  }

  async confirmStep(step) {
    console.log(step.options);
    const txDetails = step.options;

    txDetails.datetime = step.result;
    await step.context.sendActivity(`Got it: ${txDetails.amount}€ for ${txDetails.products} at ${txDetails.payee} (${txDetails.datetime[0].timex}).`);
    return await step.prompt(CONFIRM_PROMPT, 'Is that correct?', ['yes', 'no']);
  }

  async summaryStep(step) {
    console.log(step.options);
    if (step.result) {
      const txDetails = step.options;

      const { bank } = this;
      const payeeAccount = await this.getPayeeAccount(txDetails.payee);
      console.log('payeeAccount');
      console.log(payeeAccount);

      const datetime = timexToMillis(txDetails.datetime[0]);
      console.log('### datetime summarystep: ', datetime);

      const tx = bank.getAccountByName('cash').sendTx(payeeAccount.id, txDetails.amount, datetime.start);
      console.log('### Transcation\n', tx);

      // await step.context.sendActivity(JSON.stringify(bank));
      await step.context.sendActivity('Done!');
      // console.log(bank);
      return await step.endDialog();
    }
    return await step.endDialog();
  }

  async getPayeeAccount(payee) {
    const { bank } = this;
    const payeeHasAccount = bank.getAccountByName(payee);

    if (payeeHasAccount) {
      return payeeHasAccount;
    }
    return bank.createAccount(0, payee);
  }
}

module.exports.MoneySpendDialog = MoneySpendDialog;
