// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {
  ComponentDialog,
  ConfirmPrompt,
  DialogSet,
  DialogTurnStatus,
  NumberPrompt,
  TextPrompt,
  WaterfallDialog,
} = require('botbuilder-dialogs');

const CONFIRM_PROMPT = 'CONFIRM_PROMPT';
const NAME_PROMPT = 'NAME_PROMPT';
const NUMBER_PROMPT = 'NUMBER_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

class CreateBankDialog extends ComponentDialog {
  constructor(bankAccessor, logger, bank) {
    super('createBankDialog');

    this.bankAccessor = bankAccessor;

    this.logger = logger;

    this.bank = bank;

    this.addDialog(new TextPrompt(NAME_PROMPT));
    this.addDialog(new NumberPrompt(NUMBER_PROMPT));
    this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));

    this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
      this.nameStep.bind(this),
      this.numberStep.bind(this),
      this.confirmStep.bind(this),
      this.summaryStep.bind(this),
    ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
   * If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  async nameStep(step) {
    await step.context.sendActivity('First of all, we need to create a tracking account for you.');
    return await step.prompt(NAME_PROMPT, 'Please tell me the name of your account!');
  }

  async numberStep(step) {
    step.values.name = step.result;
    return await step.prompt(NUMBER_PROMPT, 'And what\'s your current balance?');
  }

  async confirmStep(step) {
    step.values.balance = step.result;
    await step.context.sendActivity(`Got ${step.values.balance}€ on ${step.values.name}.`);
    return await step.prompt(CONFIRM_PROMPT, 'Is that correct?', ['yes', 'no']);
  }

  async summaryStep(step) {
    if (step.result) {
      const acc = this.bank.createAccount(step.values.balance, step.values.name);

      this.bankAccessor.set(step.context, true);
      await step.context.sendActivity(`I've successfully created account ${acc.name} with balance of ${acc.balance}€!`);

      return await step.endDialog();
    }
    return await step.endDialog();
  }
}

module.exports.CreateBankDialog = CreateBankDialog;
